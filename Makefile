builder: jessie stretch buster
	echo "done"

%:
	mkdir -p builddir
	cp fuss-builder/Dockerfile builddir/$@
	sed -i "s+DOCKERDISTROHERE+$@+g" builddir/$@
	docker build -t registry.gitlab.com/fusslab/builder:$@ -f builddir/$@ --no-cache .

clean:
	rm -rf builddir
